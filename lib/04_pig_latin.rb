def translate(words)
  vowels = %w(a e i o)  #tests seem to not want 'u' to act as a valid vowel
  words.split(" ").map do |word|
    if starts_with_vowel?(word, vowels)
      word + 'ay'
    else
      vowel_index = find_first_vowel(word, vowels)
      word[vowel_index..-1] + word[0...vowel_index] + 'ay'
    end
  end.join(" ")

end


def starts_with_vowel?(word, vowels)
  vowels.include?(word[0])
end


def find_first_vowel(word, vowels)
  word.split("").each_with_index { |c, i| return i if vowels.include?(c) }
end
