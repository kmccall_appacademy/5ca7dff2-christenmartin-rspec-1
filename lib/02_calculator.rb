def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  array.inject(0) {|count, num| count + num}
end

def multiply(array)
  array.inject {|product, num| product *= num}
end

def power(num1, num2) #2, 3
  powered = 1
  num2.times {powered *= num1}
  powered
end

def factorial(num)
  factorial = num

  while num >= 2
    num -= 1
    factorial *= num
  end
  
  factorial
end
