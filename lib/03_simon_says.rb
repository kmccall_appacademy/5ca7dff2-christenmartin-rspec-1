def echo(words)
  words
end

def shout(words)
  words = words.upcase
end

def repeat(words, times=2)
  repeats = []

  times.times {repeats << words}

  repeats.join(" ")
end

def start_of_word(words, num_chars)  #s = "abcdefg"   1= a. 2 = ab 3 = abc
  words = words.split("")
  words[0..num_chars - 1].join("")
end

def first_word(words)
  words.split[0]
end

def titleize(words)
  little_words = %w(a an the and but for over or nor on at to from by)

  words.split(" ").map.each_with_index do |word, idx|
    if little_words.include?(word) && idx != 0
      word.downcase
    else
      word.capitalize
    end
  end.join(" ")

end
